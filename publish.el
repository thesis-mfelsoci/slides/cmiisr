(require 'org)
;; Global publishing functions
(require 'ox-publish)
;; LaTeX publishing functions
(require 'ox-latex)
;; Support for citations
(require 'oc)
(require 'oc-biblatex)

;; Load presets.
(load-file "compose-publish/latex-publish.el")
(load-file "compose-publish/babel.el")

;; Force publishing of unchanged files to make sure all the pages get published.
;; Otherwise, the files considered unmodified based on Org timestamps are not
;; published even if they were previously deleted from the publishing directory.
(setq org-publish-use-timestamps-flag nil)

;; Enable Babel code evaluation.
(setq org-export-babel-evaluate t)

;; Load languages for code block evaluation.
(org-babel-do-load-languages
 'org-babel-load-languages
 '((dot . t)))

;; Use BibLaTeX citation processor by default.
(setq org-cite-export-processors '((t biblatex)))

;; Load additional LaTeX packages allowing us to:
;;   - use scalable vector graphics (without exporting them through LaTeX and
;;     keeping the original document size, i.e. without cropping to picture),
(add-to-list 'org-latex-packages-alist
             '("inkscapelatex = false, inkscapearea = page" "svg"))
;;   - set up French as main document language,
(add-to-list 'org-latex-packages-alist '("french" "babel"))
;;   - configure custom colors,
(add-to-list 'org-latex-packages-alist '("" "xcolor"))
;;   - access additional math commands,
(add-to-list 'org-latex-packages-alist '("" "mathtools"))
;;   - draw TikZ pictures,
(add-to-list 'org-latex-packages-alist '("" "tikz"))
;;   - typeset border matrices,
(add-to-list 'org-latex-packages-alist '("" "kbordermatrix"))
;;   - use additional symbols,
(add-to-list 'org-latex-packages-alist '("" "tikzsymbols"))
;;   - use the simple Inria Beamer theme with the logos of the sponsors.
(add-to-list 'org-latex-packages-alist '("sponsors" "beamerthemeinria"))

;; Configure publishing.
(setq org-publish-project-alist
      (list
       (list "cmiisr"
             :base-directory "."
             :base-extension "org"
             :exclude ".*"
             :include ["cmiisr.org"]
             :publishing-function '(org-beamer-publish-to-pdf-verbose-on-error)
             :publishing-directory "./public")
       (list "cmiisr-figures"
             :base-directory "./figures"
             :base-extension "tex"
             :publishing-function '(latex-publish-to-pdf)
             :publishing-directory "./figures")))

(provide 'publish)
