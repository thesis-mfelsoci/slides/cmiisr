# « Pourquoi les avions font-ils autant de bruit ? » ou quand le calcul haute-performance s'invite en aéroacoustique

[![pipeline status](https://gitlab.inria.fr/thesis-mfelsoci/slides/cmiisr/badges/principale/pipeline.svg)](https://gitlab.inria.fr/thesis-mfelsoci/slides/cmiisr/-/commits/principale)

Présentation de vulgarisation scientifique à l'occasion de la visite des étudiants en première année du cursus CMI ISR dans l'équipe CAMUS/ICPS à ICube.

[Diaporama](https://thesis-mfelsoci.gitlabpages.inria.fr/slides/cmiisr/cmiisr.pdf)

